/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.connect;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InsertDatabase {
        public static void main(String[] args) {
            
        Connection conn = null;
        String url = "jdbc:sqlite:C:/Code/Softdev/Database/Week 1/Database/dcoffee.db";
        
        try{
            conn = DriverManager.getConnection(url);
             System.out.println("Database Connected");
        } catch (SQLException ex ){
            System.out.println(ex.getMessage());
            return;
         }
        
        
        String sql = "INSERT INTO category(category_id, category_name) VALUES (?,? )";
        try {       
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,3);
            stmt.setString(2,"candy");
            int status = stmt.executeUpdate(sql);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (conn != null) {
            try {
                    conn.close();

                } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
